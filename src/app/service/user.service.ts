import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {User} from '../model/user.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: Http) { }

  getUsers(): Observable<any> {
    return this.http.get('https://api.myjson.com/bins/brvdc');
  }
}
