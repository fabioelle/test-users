import { Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {UserService} from '../../service/user.service';
import {User} from '../../model/user.model';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {
  dataSource: MatTableDataSource<User>;
  displayedColumns: string[] = ['first', 'last', 'phone', 'location'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe((res) => {
      const result = res.json();
      const dataTemp: User[] = result.data.map((el) => {
        const userTemp: User = {
          first: el.user.first_name,
          last: el.user.last_name,
          phone: el.user.phone,
          location: el.site.name
        };
        return userTemp;
      });
      this.dataSource = new MatTableDataSource<User>(dataTemp);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => {
      console.log(error);
    });
  }
}
